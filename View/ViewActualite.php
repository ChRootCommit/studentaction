<!DOCTYPE>
<html>
    
    <head>
        
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css"  href="View/style/style.css" />
        <meta charset="utf-8" />
        <title>Student Action</title>
    </head>

    <body>
    
        <?php include("View/include/menu.php"); ?>
        <section>
            <h1 class="actuH1">Actualité </h1>
            <hr/>
            <?php
            if(isset($ListeArticles)){
                foreach($ListeArticles as $art){
                    $tempContent = substr($art->contenu, 0, 125);
                    echo '<div class="jumbotron jumbotron-size"';
                    echo '<h1 class="display-4">'.$art->title.'</h1>';
                    echo '<p class="lead">'.$tempContent.'</p>';
                    echo '<hr class="my-4">';
                    echo '<a class="btn btn-primary btn-lg" href="#" role="button">Lire la suite</a>';
                    echo '</div>';
                    echo '<hr/>';
                }

            }

            ?>
            <nav aria-label="Page navigation example">
                <ul class="pagination">
                    <li class="page-item"><a class="page-link" href="#">Previous</a></li>
                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item"><a class="page-link" href="#">Next</a></li>
                </ul>
            </nav>
        </section>
        

    <?php include("View/include/footer.php"); ?>
    </body>

</html>