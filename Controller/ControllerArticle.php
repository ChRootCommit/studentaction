<?php 

    include_once("Modele/ModeleArticle.php");

    class ControllerArticle{
        private $ModeleArticle;

        public function __construct(){
            $this->ModeleArticle = new ModeleArticle();
        }

        public function getListeArticles(){
            $ListeArticles = $this->ModeleArticle->getListeArticles();
            include_once("View/ViewActualite.php");
        }

    }



?>