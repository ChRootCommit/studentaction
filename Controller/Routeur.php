<?php
require_once 'Controller/ControllerUtilisateur.php';
require_once 'Controller/ControllerGroupe.php';
require_once 'Controller/ControllerAccueil.php';
require_once 'Controller/ControllerArticle.php';
require_once 'Controller/ControllerVideo.php';
require_once 'Controller/ControllerSignup.php';

class Routeur {
    // Route une requête entrante : exécution la bonne méthode de contrôleur en fonction de l'URL 
    public function routerRequete() {
		// s'il y a un parametre 'route'
        if (!empty($_GET['home'])) {
            // on détermine avec quelle méthode de quel contrôleur on veut travailler
            $RouteGet = htmlspecialchars($_GET['home']);
			switch($RouteGet) {
				case 'video':
					$ControllerVideo = new ControllerVideo();
					$ControllerVideo->getVideo();
					break;
				case 'vision':
					break;
				case 'mission':
					break;
				
				case 'ambition':
					break;
				case 'presentation':

					break;

				case 'consultTutorat' :   // Consultatation
					break;
                case 'consultEvent': // consultation event
					break;	  
				case 'consult': //Consultation actualité
					break;
				case 'actu': //Actualité
					$controllerArticle = new ControllerArticle();
					$controllerArticle->getListeArticles();
					include_once("View/ViewActualite.php");
					break;
				case 'soutient': //Soutenez nous
					break;
				case 'engagePro':
					break;
				case 'engageAncien':
					break;
				case 'tutorat':
					break;
				case 'nosPartenaires':
					break;

				case 'temoignage':
					break;
				case 'annuaire':
					break;
				case 'calendrier':
					break;
				case 'contact':
					break;
                case 'signup':
                    $controllerSignup = new ControllerSignup();
                    $controllerSignup->displaySignup();
                    break;
										  
				default: 	              // pour toutes les autres valeurs, on affiche la liste des groupes 
										  $ctrlCategorie = new ControleurCategorie();
									      $ctrlCategorie->getListeCategories();
									      break;			
			}
		}
		// aucune paramètre 'route' : on affiche la liste des categories
        else {  
			$ControllerAccueil = new ControllerAccueil();
			$ControllerAccueil->Afficher();
        }          
    }
}
?>