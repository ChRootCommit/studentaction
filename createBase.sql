DROP TABLE Temoignages;
DROP TABLE Articles;
DROP TABLE Utilisateurs;
DROP TABLE Promotions;
DROP TABLE Groupes;


CREATE TABLE Groupes(
	idGroupe MEDIUMINT NOT NULL AUTO_INCREMENT,
    nomGroupe VARCHAR(32) NOT NULL,
    CONSTRAINT PK_GROUPE PRIMARY KEY (idGroupe)
)Engine = INNODB;

CREATE TABLE Promotions(
    idPromotion MEDIUMINT NOT NULL AUTO_INCREMENT,
    libellePromotion VARCHAR (32) NOT NULL,
    CONSTRAINT PK_PROMOTION PRIMARY KEY (idPromotion)

);
CREATE TABLE Utilisateurs(
    idUtilisateur MEDIUMINT NOT NULL AUTO_INCREMENT,
    idGroupe MEDIUMINT,
    email varchar (32) NOT NULL ,
    username varchar(32) NOT NULL,
    password varchar(32) NOT NULL,
    prenom varchar(32) NOT NULL,
    nom varchar(32) NOT NULL,
    description TEXT DEFAULT NULL,
    tel char(10) DEFAULT NULL,
    socialMediaLink varchar(255) DEFAULT  NULL,
    isContactVisible bit DEFAULT 0 ,
    CONSTRAINT PK_UTILISATEUR PRIMARY KEY(idUtilisateur),
    CONSTRAINT FK_GROUPE FOREIGN kEY (idGroupe) REFERENCES Groupes(idGroupe)
) Engine = INNODB;



CREATE TABLE Articles(
    idArticle MEDIUMINT NOT NULL AUTO_INCREMENT,
    title VARCHAR(32) NOT NULL,
    contenu TEXT NOT NULL,
    hasImage varchar(5),
    idUtilisateur MEDIUMINT,
    dateRelease DATETIME,
    CONSTRAINT PK_ARTICLE PRIMARY kEY (idArticle),
    CONSTRAINT FK_UTILISATEUR_ARTICLE FOREIGN KEY (idUtilisateur) REFERENCES Utilisateurs(idUtilisateur)
)Engine = INNODB;

CREATE TABLE Temoignages(
    idTemoignage MEDIUMINT NOT NULL AUTO_INCREMENT,
    temoignage TEXT,
    idUtilisateur MEDIUMINT,
    CONSTRAINT PK_TEMOIGNAGE PRIMARY KEY (idTemoignage),
    CONSTRAINT FK_UTILISATEUR_TEMOI FOREIGN KEY  (idUtilisateur) REFERENCES  Utilisateurs(idUtilisateur)
) ENGINE = INNODB ;

