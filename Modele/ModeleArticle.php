<?php 
    include_once("Job/Article.php");
    include_once("connect.inc.php");

    class ModeleArticle{

        public function getArticle($idArticle){
            global $bdd;
            $req = $bdd->prepare('SELECT * FROM Articles WHERE idArticle = :idArticle');
            $res = $req->execute(array('idArticle' => $idArticle));
            $Article = new Article($res['idArticle'], $res['title'], $res['contenu'], $res['hasImage'], $res['idUtilisateur']);
            return $Article;
        }
        public function getArticlesByAuteur($idUtilisateur){
            global $bdd;
            $req = $bdd->prepare('SELECT idArticle, title, contenu, hasImage, idUtilisateur FROM Articles A , Utiliateurs U WHERE A.idUtilisateur = U.idUtilisateur AND U.idUtilisateur = :idUser');
            $req->execute(array('idUser' => $idUtilisateur));
            foreach($req as $art){
                $ListeArticles[] = new Article($art['idArticle'], $art['title'], $art['contenu'], $art['hasImage'], $art['idUtilisateur']);
            }
            return $ListeArticles;
        }

        public function getListeArticles(){
            global $bdd;
            $req = $bdd->prepare('SELECT * FROM Articles');
            $req->execute();
            $ListeArticles = [];
            foreach($req as $art){
                $ListeArticles[] = new Article($art['idArticle'], $art['title'], $art['contenu'], $art['hasImage'], $art['idUtilisateur']);
            }
            return $ListeArticles;
        }
    }


?>