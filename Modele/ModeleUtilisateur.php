
<?php 
    include_once("Job/Utilisateur.php");
    include_once("connect.inc.php");
    class ModeleUtilisateur{
    public function getUtilisateur($idUser){
        global $bdd;
        $req = $bdd -> prepare("SELECT * FROM Utilisateurs WHERE idUtilisateur = :idUser");
        $res = $req->execute(array('idUser'=>$idUser));
        $Utilisateur = new Utilisateur($res['idUtilisateur'], $res['idGroupe'],$res['username'], $res['password'], $res['prenom'], $res['nom'], $res['email']);
        return $Utilisateur;
    }

    public function getListeUtilisateurs(){
        global $bdd;
        $req = $bdd -> prepare("SELECT $ FROM Utilisateurs");
        $res = $req->execute();
        foreach($res as $user){
            $ListeUtilisateurs[] = new Utilisateur($user['idUtilisateur'], $user['idGroupe'], $user['username'], $user['password'],
                                                $user['nom'], $user['email']);
        }
        return $ListeUtilisateurs;
    }
    }

?>