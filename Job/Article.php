
<?php 

    class Article{
        public $idArticle;
        public $title;
        public $contenu;
        public $hasImage;
        public $idAuteur;

        public function __construct($idArticle, $title, $contenu, $hasImage, $idAuteur){
            $this->idArticle = $idArticle;
            $this->title = $title;
            $this->contenu = $contenu;
            $this->hasImage = $hasImage;
            $this->idAuteur = $idAuteur;
        }
    }

?>